package com.android.nsco.music;

import com.android.nsco.music.MusicUtils.ServiceToken;

import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.webkit.WebView.FindListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.ExpandableListView.OnChildClickListener;

public class ArtistAlbumBrowserFragment extends Fragment
		implements View.OnCreateContextMenuListener, MusicUtils.Defs, ServiceConnection {

	private String mCurrentArtistId;
	private String mCurrentArtistName;
	private String mCurrentAlbumId;
	private String mCurrentAlbumName;
	private String mCurrentArtistNameForAlbum;
	boolean mIsUnknownArtist;
	boolean mIsUnknownAlbum;
	private ArtistAlbumListAdapter mAdapter;
	private boolean mAdapterSent;
	private final static int SEARCH = CHILD_MENU_BASE;
	private static int mLastListPosCourse = -1;
	private static int mLastListPosFine = -1;
	private ServiceToken mToken;
	
	public View mView;

	@Override
	public void onAttach(Activity activity) {
		Log.d("TEST", getClass().getSimpleName() + "::onAttach::start.");
		super.onAttach(activity);
		Log.d("TEST", getClass().getSimpleName() + "::onAttach::end.");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("TEST", getClass().getSimpleName() + "::onCreate::start.");
		super.onCreate(savedInstanceState);
		Log.d("TEST", getClass().getSimpleName() + "::onCreate::end.");
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("TEST", getClass().getSimpleName() + "::onCreateView::start.");
		this.mView = inflater.inflate(R.layout.artist_fragment, container, false);
		Log.d("TEST", getClass().getSimpleName() + "::onCreateView::end.");
		return this.mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::start.");
		super.onActivityCreated(savedInstanceState);

//		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
		if (savedInstanceState != null) {
			mCurrentAlbumId = savedInstanceState.getString("selectedalbum");
			mCurrentAlbumName = savedInstanceState.getString("selectedalbumname");
			mCurrentArtistId = savedInstanceState.getString("selectedartist");
			mCurrentArtistName = savedInstanceState.getString("selectedartistname");
		}
		mToken = MusicUtils.bindToService(getActivity(), this);

		final IntentFilter f = new IntentFilter();
		f.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
		f.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
		f.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
		f.addDataScheme("file");
		getActivity().registerReceiver(mScanListener, f);

		final ExpandableListView lv = (ExpandableListView) this.mView.findViewById(android.R.id.list);
		lv.setOnCreateContextMenuListener(this);
		lv.setTextFilterEnabled(true);
		lv.setOnChildClickListener(this.mOnChildClickListener);
		
		mAdapter = (ArtistAlbumListAdapter) getActivity().getLastNonConfigurationInstance();
		if (mAdapter == null) {
			Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::null == mAdapter::start");
			//Log.i("@@@", "starting query");
			mAdapter = new ArtistAlbumListAdapter(
					getActivity().getApplication(),
					this,
					null, // cursor
					R.layout.track_list_item_group,
					new String[] {},
					new int[] {},
					R.layout.track_list_item_child,
					new String[] {},
					new int[] {});
			lv.setAdapter(mAdapter);
			getArtistCursor(mAdapter.getQueryHandler(), null);
			Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::null == mAdapter::end");
		} else {
			Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::null != mAdapter::start");
			mAdapter.setActivity(this);
			lv.setAdapter(mAdapter);
			mArtistCursor = mAdapter.getCursor();
			if (mArtistCursor != null) {
				init(mArtistCursor);
			} else {
				getArtistCursor(mAdapter.getQueryHandler(), null);
			}
			Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::null != mAdapter::end");
		}
		Log.d("TEST", getClass().getSimpleName() + "::onActivityCreated::end.");
	}

	@Override
	public void onStart() {
		Log.d("TEST", getClass().getSimpleName() + "::onStart::start.");
		super.onStart();
		Log.d("TEST", getClass().getSimpleName() + "::onStart::end.");
	}

	@Override
	public void onResume() {
		Log.d("TEST", getClass().getSimpleName() + "::onResume::start.");
		super.onResume();

        IntentFilter f = new IntentFilter();
        f.addAction(MediaPlaybackService.META_CHANGED);
        f.addAction(MediaPlaybackService.QUEUE_CHANGED);
        getActivity().registerReceiver(mTrackListListener, f);
        mTrackListListener.onReceive(null, null);

        MusicUtils.setSpinnerState(getActivity());
		
		Log.d("TEST", getClass().getSimpleName() + "::onResume::end.");
	}

	@Override
	public void onPause() {
		Log.d("TEST", getClass().getSimpleName() + "::onPause::start.");
		super.onPause();

        getActivity().unregisterReceiver(mTrackListListener);
        mReScanHandler.removeCallbacksAndMessages(null);
        
		Log.d("TEST", getClass().getSimpleName() + "::onPause::end.");
	}

	@Override
	public void onStop() {
		Log.d("TEST", getClass().getSimpleName() + "::onStop::start.");
		super.onStop();
		Log.d("TEST", getClass().getSimpleName() + "::onStop::end.");
	}

	@Override
	public void onDestroyView() {
		Log.d("TEST", getClass().getSimpleName() + "::onDestroyView::start.");
		super.onDestroyView();
		Log.d("TEST", getClass().getSimpleName() + "::onDestroyView::end.");
	}

	@Override
	public void onDestroy() {
		Log.d("TEST", getClass().getSimpleName() + "::onDestroy::start.");
		super.onDestroy();
		Log.d("TEST", getClass().getSimpleName() + "::onDestroy::end.");
	}

	@Override
	public void onDetach() {
		Log.d("TEST", getClass().getSimpleName() + "::onDetach::start.");
		super.onDetach();

		final ExpandableListView lv = (ExpandableListView) this.mView.findViewById(android.R.id.list);
        if (lv != null) {
            mLastListPosCourse = lv.getFirstVisiblePosition();
            View cv = lv.getChildAt(0);
            if (cv != null) {
                mLastListPosFine = cv.getTop();
            }
        }
        
        MusicUtils.unbindFromService(mToken);
        // If we have an adapter and didn't send it off to another activity yet, we should
        // close its cursor, which we do by assigning a null cursor to it. Doing this
        // instead of closing the cursor directly keeps the framework from accessing
        // the closed cursor later.
        if (!mAdapterSent && mAdapter != null) {
            mAdapter.changeCursor(null);
        }
        // Because we pass the adapter to the next activity, we need to make
        // sure it doesn't keep a reference to this activity. We can do this
        // by clearing its DatasetObservers, which setListAdapter(null) does.
        mAdapter = null;
        getActivity().unregisterReceiver(mScanListener);
        lv.setAdapter(mAdapter);
		
		Log.d("TEST", getClass().getSimpleName() + "::onDetach::end.");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putString("selectedalbum", mCurrentAlbumId);
		outState.putString("selectedalbumname", mCurrentAlbumName);
		outState.putString("selectedartist", mCurrentArtistId);
		outState.putString("selectedartistname", mCurrentArtistName);
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		Log.d("TEST", getClass().getSimpleName() + "::onCreateOptionsMenu::start.");
		super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, PARTY_SHUFFLE, 0, R.string.party_shuffle); // icon will be set in onPrepareOptionsMenu()
        menu.add(0, SHUFFLE_ALL, 0, R.string.shuffle_all).setIcon(R.drawable.ic_menu_shuffle);
		Log.d("TEST", getClass().getSimpleName() + "::onCreateOptionsMenu::end.");
	}
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		Log.d("TEST", getClass().getSimpleName() + "::onPrepareOptionsMenu::start.");
		super.onPrepareOptionsMenu(menu);
        MusicUtils.setPartyShuffleMenuIcon(menu);
		Log.d("TEST", getClass().getSimpleName() + "::onPrepareOptionsMenu::end.");
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        Cursor cursor;
        switch (item.getItemId()) {
            case PARTY_SHUFFLE:
                MusicUtils.togglePartyShuffle();
                break;
                
            case SHUFFLE_ALL:
                cursor = MusicUtils.query(getActivity(), MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        new String [] { MediaStore.Audio.Media._ID}, 
                        MediaStore.Audio.Media.IS_MUSIC + "=1", null,
                        MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
                if (cursor != null) {
                    MusicUtils.shuffleAll(getActivity(), cursor);
                    cursor.close();
                }
                return true;
        }
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        menu.add(0, PLAY_SELECTION, 0, R.string.play_selection);
        SubMenu sub = menu.addSubMenu(0, ADD_TO_PLAYLIST, 0, R.string.add_to_playlist);
        MusicUtils.makePlaylistMenu(getActivity(), sub);
        menu.add(0, DELETE_ITEM, 0, R.string.delete_item);

		final ExpandableListView lv = (ExpandableListView) this.mView.findViewById(android.R.id.list);
        ExpandableListContextMenuInfo mi = (ExpandableListContextMenuInfo) menuInfo;
        
        int itemtype = ExpandableListView.getPackedPositionType(mi.packedPosition);
        int gpos = ExpandableListView.getPackedPositionGroup(mi.packedPosition);
        int cpos = ExpandableListView.getPackedPositionChild(mi.packedPosition);
        if (itemtype == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            if (gpos == -1) {
                // this shouldn't happen
                Log.d("Artist/Album", "no group");
                return;
            }
            gpos = gpos - lv.getHeaderViewsCount();
            mArtistCursor.moveToPosition(gpos);
            mCurrentArtistId = mArtistCursor.getString(mArtistCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists._ID));
            mCurrentArtistName = mArtistCursor.getString(mArtistCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST));
            mCurrentAlbumId = null;
            mIsUnknownArtist = mCurrentArtistName == null ||
                    mCurrentArtistName.equals(MediaStore.UNKNOWN_STRING);
            mIsUnknownAlbum = true;
            if (mIsUnknownArtist) {
                menu.setHeaderTitle(getString(R.string.unknown_artist_name));
            } else {
                menu.setHeaderTitle(mCurrentArtistName);
                menu.add(0, SEARCH, 0, R.string.search_title);
            }
            return;
        } else if (itemtype == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            if (cpos == -1) {
                // this shouldn't happen
                Log.d("Artist/Album", "no child");
                return;
            }
            Cursor c = (Cursor) lv.getExpandableListAdapter().getChild(gpos, cpos);
            c.moveToPosition(cpos);
            mCurrentArtistId = null;
            mCurrentAlbumId = Long.valueOf(mi.id).toString();
            mCurrentAlbumName = c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
            gpos = gpos - lv.getHeaderViewsCount();
            mArtistCursor.moveToPosition(gpos);
            mCurrentArtistNameForAlbum = mArtistCursor.getString(
                    mArtistCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST));
            mIsUnknownArtist = mCurrentArtistNameForAlbum == null ||
                    mCurrentArtistNameForAlbum.equals(MediaStore.UNKNOWN_STRING);
            mIsUnknownAlbum = mCurrentAlbumName == null ||
                    mCurrentAlbumName.equals(MediaStore.UNKNOWN_STRING);
            if (mIsUnknownAlbum) {
                menu.setHeaderTitle(getString(R.string.unknown_album_name));
            } else {
                menu.setHeaderTitle(mCurrentAlbumName);
            }
            if (!mIsUnknownAlbum || !mIsUnknownArtist) {
                menu.add(0, SEARCH, 0, R.string.search_title);
            }
        }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case PLAY_SELECTION: {
			// play everything by the selected artist
			long[] list = mCurrentArtistId != null
					? MusicUtils.getSongListForArtist(getActivity(), Long.parseLong(mCurrentArtistId))
					: MusicUtils.getSongListForAlbum(getActivity(), Long.parseLong(mCurrentAlbumId));

			MusicUtils.playAll(getActivity(), list, 0);
			return true;
		}

		case QUEUE: {
			long[] list = mCurrentArtistId != null
					? MusicUtils.getSongListForArtist(getActivity(), Long.parseLong(mCurrentArtistId))
					: MusicUtils.getSongListForAlbum(getActivity(), Long.parseLong(mCurrentAlbumId));
			MusicUtils.addToCurrentPlaylist(getActivity(), list);
			return true;
		}

		case NEW_PLAYLIST: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), CreatePlaylist.class);
			startActivityForResult(intent, NEW_PLAYLIST);
			return true;
		}

		case PLAYLIST_SELECTED: {
			long[] list = mCurrentArtistId != null
					? MusicUtils.getSongListForArtist(getActivity(), Long.parseLong(mCurrentArtistId))
					: MusicUtils.getSongListForAlbum(getActivity(), Long.parseLong(mCurrentAlbumId));
			long playlist = item.getIntent().getLongExtra("playlist", 0);
			MusicUtils.addToPlaylist(getActivity(), list, playlist);
			return true;
		}

		case DELETE_ITEM: {
			long[] list;
			String desc;
			if (mCurrentArtistId != null) {
				list = MusicUtils.getSongListForArtist(getActivity(), Long.parseLong(mCurrentArtistId));
				String f;
				if (android.os.Environment.isExternalStorageRemovable()) {
					f = getString(R.string.delete_artist_desc);
				} else {
					f = getString(R.string.delete_artist_desc_nosdcard);
				}
				desc = String.format(f, mCurrentArtistName);
			} else {
				list = MusicUtils.getSongListForAlbum(getActivity(), Long.parseLong(mCurrentAlbumId));
				String f;
				if (android.os.Environment.isExternalStorageRemovable()) {
					f = getString(R.string.delete_album_desc);
				} else {
					f = getString(R.string.delete_album_desc_nosdcard);
				}

				desc = String.format(f, mCurrentAlbumName);
			}
			Bundle b = new Bundle();
			b.putString("description", desc);
			b.putLongArray("items", list);
			Intent intent = new Intent();
			intent.setClass(getActivity(), DeleteItems.class);
			intent.putExtras(b);
			startActivityForResult(intent, -1);
			return true;
		}

		case SEARCH:
			doSearch();
			return true;
		}
		return super.onContextItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
        case SCAN_DONE:
            if (resultCode == Activity.RESULT_CANCELED) {
//                finish();
            } else {
                getArtistCursor(mAdapter.getQueryHandler(), null);
            }
            break;

        case NEW_PLAYLIST:
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = intent.getData();
                if (uri != null) {
                    long [] list = null;
                    if (mCurrentArtistId != null) {
                        list = MusicUtils.getSongListForArtist(getActivity(), Long.parseLong(mCurrentArtistId));
                    } else if (mCurrentAlbumId != null) {
                        list = MusicUtils.getSongListForAlbum(getActivity(), Long.parseLong(mCurrentAlbumId));
                    }
                    MusicUtils.addToPlaylist(getActivity(), list, Long.parseLong(uri.getLastPathSegment()));
                }
            }
            break;
        }
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		Log.d("TEST", getClass().getSimpleName() + "::onServiceConnected::start.");
        MusicUtils.updateNowPlaying(getActivity());
		Log.d("TEST", getClass().getSimpleName() + "::onServiceConnected::end.");
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		Log.d("TEST", getClass().getSimpleName() + "::onServiceDisconnected::start.");
//		finish();
		Log.d("TEST", getClass().getSimpleName() + "::onServiceDisconnected::end.");
	}
	
	OnChildClickListener mOnChildClickListener = new OnChildClickListener() {
		
		@Override
		public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
	        mCurrentAlbumId = Long.valueOf(id).toString();
	        
	        Intent intent = new Intent(Intent.ACTION_PICK);
	        intent.setDataAndType(Uri.EMPTY, "vnd.android.cursor.dir/track");
	        intent.putExtra("album", mCurrentAlbumId);

			final ExpandableListView lv = (ExpandableListView) mView.findViewById(android.R.id.list);
	        Cursor c = (Cursor) lv.getExpandableListAdapter().getChild(groupPosition, childPosition);
	        String album = c.getString(c.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
	        if (album == null || album.equals(MediaStore.UNKNOWN_STRING)) {
	            // unknown album, so we should include the artist ID to limit the songs to songs only by that artist 
	            mArtistCursor.moveToPosition(groupPosition);
	            mCurrentArtistId = mArtistCursor.getString(mArtistCursor.getColumnIndex(MediaStore.Audio.Artists._ID));
	            intent.putExtra("artist", mCurrentArtistId);
	        }
	        startActivity(intent);
	        return true;
		}
	};
	
    BroadcastReceiver mScanListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
    		Log.d("TEST", getClass().getSimpleName() + "::onReceive::start.");
//            MusicUtils.setSpinnerState(ArtistAlbumBrowserActivity.this);
//            mReScanHandler.sendEmptyMessage(0);
//            if (intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
//                MusicUtils.clearAlbumArtCache();
//            }
    		Log.d("TEST", getClass().getSimpleName() + "::onReceive::end.");
        }
    };
    

    BroadcastReceiver mTrackListListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

    		final ExpandableListView lv = (ExpandableListView) mView.findViewById(android.R.id.list);
    		lv.invalidateViews();
            MusicUtils.updateNowPlaying(getActivity());
        }
    };
    
    private Handler mReScanHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
    		Log.d("TEST", getClass().getSimpleName() + "::handleMessage::start.");
            if (mAdapter != null) {
                getArtistCursor(mAdapter.getQueryHandler(), null);
            }
    		Log.d("TEST", getClass().getSimpleName() + "::handleMessage::start.");
        }
    };



    void doSearch() {
        CharSequence title = null;
        String query = null;
        
        Intent i = new Intent();
        i.setAction(MediaStore.INTENT_ACTION_MEDIA_SEARCH);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        if (mCurrentArtistId != null) {
            title = mCurrentArtistName;
            query = mCurrentArtistName;
            i.putExtra(MediaStore.EXTRA_MEDIA_ARTIST, mCurrentArtistName);
            i.putExtra(MediaStore.EXTRA_MEDIA_FOCUS, MediaStore.Audio.Artists.ENTRY_CONTENT_TYPE);
        } else {
            if (mIsUnknownAlbum) {
                title = query = mCurrentArtistNameForAlbum;
            } else {
                title = query = mCurrentAlbumName;
                if (!mIsUnknownArtist) {
                    query = query + " " + mCurrentArtistNameForAlbum;
                }
            }
            i.putExtra(MediaStore.EXTRA_MEDIA_ARTIST, mCurrentArtistNameForAlbum);
            i.putExtra(MediaStore.EXTRA_MEDIA_ALBUM, mCurrentAlbumName);
            i.putExtra(MediaStore.EXTRA_MEDIA_FOCUS, MediaStore.Audio.Albums.ENTRY_CONTENT_TYPE);
        }
        title = getString(R.string.mediasearch, title);
        i.putExtra(SearchManager.QUERY, query);

        startActivity(Intent.createChooser(i, title));
    }
    
    public void init(Cursor c) {

        if (mAdapter == null) {
            return;
        }
        mAdapter.changeCursor(c); // also sets mArtistCursor

        if (mArtistCursor == null) {
            MusicUtils.displayDatabaseError(getActivity());
            getActivity().closeContextMenu();
            mReScanHandler.sendEmptyMessageDelayed(0, 1000);
            return;
        }

        // restore previous position
        if (mLastListPosCourse >= 0) {
        	// TODO 一時的にコメントアウト
//            ExpandableListView elv = getExpandableListView();
//            elv.setSelectionFromTop(mLastListPosCourse, mLastListPosFine);
//            mLastListPosCourse = -1;
        }

        MusicUtils.hideDatabaseError(getActivity());
    }

    private Cursor getArtistCursor(AsyncQueryHandler async, String filter) {

        String[] cols = new String[] {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS,
                MediaStore.Audio.Artists.NUMBER_OF_TRACKS
        };

        Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        if (!TextUtils.isEmpty(filter)) {
            uri = uri.buildUpon().appendQueryParameter("filter", Uri.encode(filter)).build();
        }

        Cursor ret = null;
        if (async != null) {
            async.startQuery(0, null, uri,
                    cols, null , null, MediaStore.Audio.Artists.ARTIST_KEY);
        } else {
            ret = MusicUtils.query(getActivity(), uri,
                    cols, null , null, MediaStore.Audio.Artists.ARTIST_KEY);
        }
        return ret;
    }

    static class ArtistAlbumListAdapter extends SimpleCursorTreeAdapter implements SectionIndexer {
        
        private final Drawable mNowPlayingOverlay;
        private final BitmapDrawable mDefaultAlbumIcon;
        private int mGroupArtistIdIdx;
        private int mGroupArtistIdx;
        private int mGroupAlbumIdx;
        private int mGroupSongIdx;
        private final Context mContext;
        private final Resources mResources;
        private final String mAlbumSongSeparator;
        private final String mUnknownAlbum;
        private final String mUnknownArtist;
        private final StringBuilder mBuffer = new StringBuilder();
        private final Object[] mFormatArgs = new Object[1];
        private final Object[] mFormatArgs3 = new Object[3];
        private MusicAlphabetIndexer mIndexer;
        private ArtistAlbumBrowserFragment mActivity;
        private AsyncQueryHandler mQueryHandler;
        private String mConstraint = null;
        private boolean mConstraintIsValid = false;
        
        static class ViewHolder {
            TextView line1;
            TextView line2;
            ImageView play_indicator;
            ImageView icon;
        }

        class QueryHandler extends AsyncQueryHandler {
            QueryHandler(ContentResolver res) {
                super(res);
            }
            
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                //Log.i("@@@", "query complete");
                mActivity.init(cursor);
            }
        }

        ArtistAlbumListAdapter(Context context, ArtistAlbumBrowserFragment fragment,
                Cursor cursor, int glayout, String[] gfrom, int[] gto, 
                int clayout, String[] cfrom, int[] cto) {
            super(context, cursor, glayout, gfrom, gto, clayout, cfrom, cto);
            mActivity = fragment;
            mQueryHandler = new QueryHandler(context.getContentResolver());

            Resources r = context.getResources();
            mNowPlayingOverlay = r.getDrawable(R.drawable.indicator_ic_mp_playing_list);
            mDefaultAlbumIcon = (BitmapDrawable) r.getDrawable(R.drawable.albumart_mp_unknown_list);
            // no filter or dither, it's a lot faster and we can't tell the difference
            mDefaultAlbumIcon.setFilterBitmap(false);
            mDefaultAlbumIcon.setDither(false);
            
            mContext = context;
            getColumnIndices(cursor);
            mResources = context.getResources();
            mAlbumSongSeparator = context.getString(R.string.albumsongseparator);
            mUnknownAlbum = context.getString(R.string.unknown_album_name);
            mUnknownArtist = context.getString(R.string.unknown_artist_name);
        }
        
        private void getColumnIndices(Cursor cursor) {
            if (cursor != null) {
                mGroupArtistIdIdx = cursor.getColumnIndexOrThrow(MediaStore.Audio.Artists._ID);
                mGroupArtistIdx = cursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST);
                mGroupAlbumIdx = cursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS);
                mGroupSongIdx = cursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.NUMBER_OF_TRACKS);
                if (mIndexer != null) {
                    mIndexer.setCursor(cursor);
                } else {
                    mIndexer = new MusicAlphabetIndexer(cursor, mGroupArtistIdx, 
                            mResources.getString(R.string.fast_scroll_alphabet));
                }
            }
        }
        
        public void setActivity(ArtistAlbumBrowserFragment fragment) {
            mActivity = fragment;
        }
        
        public AsyncQueryHandler getQueryHandler() {
            return mQueryHandler;
        }

        @Override
        public View newGroupView(Context context, Cursor cursor, boolean isExpanded, ViewGroup parent) {
            View v = super.newGroupView(context, cursor, isExpanded, parent);
            ImageView iv = (ImageView) v.findViewById(R.id.icon);
            ViewGroup.LayoutParams p = iv.getLayoutParams();
            p.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            p.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            ViewHolder vh = new ViewHolder();
            vh.line1 = (TextView) v.findViewById(R.id.line1);
            vh.line2 = (TextView) v.findViewById(R.id.line2);
            vh.play_indicator = (ImageView) v.findViewById(R.id.play_indicator);
            vh.icon = (ImageView) v.findViewById(R.id.icon);
            vh.icon.setPadding(0, 0, 1, 0);
            v.setTag(vh);
            return v;
        }

        @Override
        public View newChildView(Context context, Cursor cursor, boolean isLastChild,
                ViewGroup parent) {
            View v = super.newChildView(context, cursor, isLastChild, parent);
            ViewHolder vh = new ViewHolder();
            vh.line1 = (TextView) v.findViewById(R.id.line1);
            vh.line2 = (TextView) v.findViewById(R.id.line2);
            vh.play_indicator = (ImageView) v.findViewById(R.id.play_indicator);
            vh.icon = (ImageView) v.findViewById(R.id.icon);
            vh.icon.setBackgroundDrawable(mDefaultAlbumIcon);
            vh.icon.setPadding(0, 0, 1, 0);
            v.setTag(vh);
            return v;
        }
        
        @Override
        public void bindGroupView(View view, Context context, Cursor cursor, boolean isexpanded) {

            ViewHolder vh = (ViewHolder) view.getTag();

            String artist = cursor.getString(mGroupArtistIdx);
            String displayartist = artist;
            boolean unknown = artist == null || artist.equals(MediaStore.UNKNOWN_STRING);
            if (unknown) {
                displayartist = mUnknownArtist;
            }
            vh.line1.setText(displayartist);

            int numalbums = cursor.getInt(mGroupAlbumIdx);
            int numsongs = cursor.getInt(mGroupSongIdx);
            
            String songs_albums = MusicUtils.makeAlbumsLabel(context,
                    numalbums, numsongs, unknown);
            
            vh.line2.setText(songs_albums);
            
            long currentartistid = MusicUtils.getCurrentArtistId();
            long artistid = cursor.getLong(mGroupArtistIdIdx);
            if (currentartistid == artistid && !isexpanded) {
                vh.play_indicator.setImageDrawable(mNowPlayingOverlay);
            } else {
                vh.play_indicator.setImageDrawable(null);
            }
        }

        @Override
        public void bindChildView(View view, Context context, Cursor cursor, boolean islast) {

            ViewHolder vh = (ViewHolder) view.getTag();

            String name = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
            String displayname = name;
            boolean unknown = name == null || name.equals(MediaStore.UNKNOWN_STRING); 
            if (unknown) {
                displayname = mUnknownAlbum;
            }
            vh.line1.setText(displayname);

            int numsongs = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
            int numartistsongs = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.NUMBER_OF_SONGS_FOR_ARTIST));

            final StringBuilder builder = mBuffer;
            builder.delete(0, builder.length());
            if (unknown) {
                numsongs = numartistsongs;
            }
              
            if (numsongs == 1) {
                builder.append(context.getString(R.string.onesong));
            } else {
                if (numsongs == numartistsongs) {
                    final Object[] args = mFormatArgs;
                    args[0] = numsongs;
                    builder.append(mResources.getQuantityString(R.plurals.Nsongs, numsongs, args));
                } else {
                    final Object[] args = mFormatArgs3;
                    args[0] = numsongs;
                    args[1] = numartistsongs;
                    args[2] = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST));
                    builder.append(mResources.getQuantityString(R.plurals.Nsongscomp, numsongs, args));
                }
            }
            vh.line2.setText(builder.toString());
            
            ImageView iv = vh.icon;
            // We don't actually need the path to the thumbnail file,
            // we just use it to see if there is album art or not
            String art = cursor.getString(cursor.getColumnIndexOrThrow(
                    MediaStore.Audio.Albums.ALBUM_ART));
            if (unknown || art == null || art.length() == 0) {
                iv.setBackgroundDrawable(mDefaultAlbumIcon);
                iv.setImageDrawable(null);
            } else {
                long artIndex = cursor.getLong(0);
                Drawable d = MusicUtils.getCachedArtwork(context, artIndex, mDefaultAlbumIcon);
                iv.setImageDrawable(d);
            }

            long currentalbumid = MusicUtils.getCurrentAlbumId();
            long aid = cursor.getLong(0);
            iv = vh.play_indicator;
            if (currentalbumid == aid) {
                iv.setImageDrawable(mNowPlayingOverlay);
            } else {
                iv.setImageDrawable(null);
            }
        }

        
        @Override
        protected Cursor getChildrenCursor(Cursor groupCursor) {
            
            long id = groupCursor.getLong(groupCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists._ID));
            
            String[] cols = new String[] {
                    MediaStore.Audio.Albums._ID,
                    MediaStore.Audio.Albums.ALBUM,
                    MediaStore.Audio.Albums.NUMBER_OF_SONGS,
                    MediaStore.Audio.Albums.NUMBER_OF_SONGS_FOR_ARTIST,
                    MediaStore.Audio.Albums.ALBUM_ART
            };
            Cursor c = MusicUtils.query(mContext,
                    MediaStore.Audio.Artists.Albums.getContentUri("external", id),
                    cols, null, null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
            
            class MyCursorWrapper extends CursorWrapper {
                String mArtistName;
                int mMagicColumnIdx;
                MyCursorWrapper(Cursor c, String artist) {
                    super(c);
                    mArtistName = artist;
                    if (mArtistName == null || mArtistName.equals(MediaStore.UNKNOWN_STRING)) {
                        mArtistName = mUnknownArtist;
                    }
                    mMagicColumnIdx = c.getColumnCount();
                }
                
                @Override
                public String getString(int columnIndex) {
                    if (columnIndex != mMagicColumnIdx) {
                        return super.getString(columnIndex);
                    }
                    return mArtistName;
                }
                
                @Override
                public int getColumnIndexOrThrow(String name) {
                    if (MediaStore.Audio.Albums.ARTIST.equals(name)) {
                        return mMagicColumnIdx;
                    }
                    return super.getColumnIndexOrThrow(name); 
                }
                
                @Override
                public String getColumnName(int idx) {
                    if (idx != mMagicColumnIdx) {
                        return super.getColumnName(idx);
                    }
                    return MediaStore.Audio.Albums.ARTIST;
                }
                
                @Override
                public int getColumnCount() {
                    return super.getColumnCount() + 1;
                }
            }
            return new MyCursorWrapper(c, groupCursor.getString(mGroupArtistIdx));
        }

        @Override
        public void changeCursor(Cursor cursor) {
            if (mActivity.isDetached() && cursor != null) {
                cursor.close();
                cursor = null;
            }
            if (cursor != mActivity.mArtistCursor) {
                mActivity.mArtistCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }
        
        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            String s = constraint.toString();
            if (mConstraintIsValid && (
                    (s == null && mConstraint == null) ||
                    (s != null && s.equals(mConstraint)))) {
                return getCursor();
            }
            Cursor c = mActivity.getArtistCursor(null, s);
            mConstraint = s;
            mConstraintIsValid = true;
            return c;
        }

        public Object[] getSections() {
            return mIndexer.getSections();
        }
        
        public int getPositionForSection(int sectionIndex) {
            return mIndexer.getPositionForSection(sectionIndex);
        }
        
        public int getSectionForPosition(int position) {
            return 0;
        }
    }
    
    private Cursor mArtistCursor;
}
